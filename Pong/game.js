// game.js

var width = 800;
var height = 480;
var gLoop;
var state = true;

var canvas = document.getElementById('c').getContext("2d");
c.width = width;
c.height = height;

var p1sd = document.getElementById('player1');
var p2sd = document.getElementById('player2');

var blip = document.getElementById('blip');
var point = document.getElementById('point');

var clear = function() {
	canvas.fillStyle = '#000';
	canvas.beginPath();
	canvas.rect(0,0,width,height);
	canvas.closePath();
	canvas.fill();

	canvas.strokeStyle = '#FFF';
	canvas.lineWidth = 1;
	canvas.beginPath();
	canvas.moveTo(400,5);
	canvas.lineTo(400,475);
	canvas.closePath();
	canvas.stroke();
}

var Player = function(cX,cY,div) {
	var self = this;

	self.pWidth = 25;
	self.pHeight = 100;
	self.coordX = cX;
	self.coordY = cY;
	self.speed = 35;
	self.canUp = true;
	self.canDown = true;
	self.score = 0;
	self.scorediv = div;

	self.scoreUp = function() {
		self.score++; point.play(0);
		self.scorediv.innerHTML = "<p>"+self.score+"</p>";
		if(self.score == 7) {
			GameOver();
		}
	};

	self.moveUp = function() {
		if(self.canUp) self.coordY -= self.speed;
	};

	self.moveDown = function() {
		if(self.canDown) self.coordY += self.speed;
	};

	self.draw = function() {
		canvas.fillStyle = '#FFF';
		canvas.beginPath();
		canvas.rect(self.coordX,self.coordY,self.pWidth,self.pHeight);
		canvas.closePath();
		canvas.fill();
	};
};

var Ball = function(cX,cY) {
	var self = this;

	self.r = 20;
	self.coordX = cX;
	self.coordY = cY;
	self.speed = 15;
	self.bounceAngle = Math.PI*5/12;
	self.vx = 0.3;
	self.vy = 0.3;

	self._cX = cX;
	self._cY = cY;
	self.restart = function() {
		self.coordX = self._cX;
		self.coordY = self._cY;
		self.vx = (~~(Math.random()*2)==0) ? 0.3 : -0.3;
		self.vy = (~~(Math.random()*2)==0) ? 0.3 : -0.3;
	};

	self.update = function() {
		self.coordX += self.vx * (1000/50);
		self.coordY += self.vy * (1000/50);
	};

	self.draw = function() {
		canvas.fillStyle = '#FFF';
		canvas.beginPath();
		canvas.arc(self.coordX,self.coordY, self.r, 0, Math.PI*2, false);
		canvas.closePath();
		canvas.fill();
		self.update();
	};

};

// w 		=> 87
// s 		=> 83
// up 		=> 38
// down 	=> 40
window.onkeydown = function(e) {
	if(e.keyCode == 87) { Player1.moveUp(); return; }
	if(e.keyCode == 83) { Player1.moveDown(); return; }
	if(e.keyCode == 38) { Player2.moveUp(); return; }
	if(e.keyCode == 40) { Player2.moveDown(); return; }
	if(e.keyCode == 13 && !state) { location.reload(); }
};

var checkCollision = function() {
	if(Player1.coordY - Player1.speed < 0) Player1.canUp = false; 
	else Player1.canUp = true;
	if(Player1.coordY+Player1.pHeight+Player1.speed>height) Player1.canDown = false;
	else Player1.canDown = true;

	if(Player2.coordY - Player2.speed < 0) Player2.canUp = false; 
	else Player2.canUp = true;
	if(Player2.coordY+Player2.pHeight+Player2.speed>height) Player2.canDown = false;
	else Player2.canDown = true;

	// zderzenie z kulką
	if((Player1.coordX<=MainBall.coordX && MainBall.coordX<=Player1.coordX+Player1.pWidth)
		&& (Player1.coordY<=MainBall.coordY && MainBall.coordY<=Player1.coordY+Player1.pHeight)) {
		MainBall.vx = -MainBall.vx; blip.play();
		MainBall.coordX = Player1.coordX + Player1.pWidth + MainBall.r;
	} else if((Player2.coordX<=MainBall.coordX && MainBall.coordX<=Player2.coordX+Player2.pWidth)
		&& (Player2.coordY<=MainBall.coordY && MainBall.coordY<=Player2.coordY+Player2.pHeight)) {
		MainBall.vx = -MainBall.vx; blip.play();
		MainBall.coordX = Player2.coordX - MainBall.r;
	}
	// górna i dolna ściana
	if(MainBall.coordY+MainBall.r>height) {
		MainBall.vy = -MainBall.vy;
		MainBall.coordY = height - MainBall.r;
	} else if (MainBall.coordY-MainBall.r<0) {
		MainBall.vy = -MainBall.vy;
		MainBall.coordY = MainBall.r;
	}
	//lewa i prawa ściana
	if(MainBall.coordX<0) {
		Player2.scoreUp();
		MainBall.restart();
	} else if(MainBall.coordX>width) {
		Player1.scoreUp();
		MainBall.restart();
	}
};

var Player1 = new Player(5,190,p1sd);
var Player2 = new Player(770,190,p2sd);
var MainBall = new Ball(400,240);

var GameOver = function() {
	state = false;
	clearTimeout(gLoop);
	setTimeout(function(){
		canvas.fillStyle = '#000';
		canvas.beginPath();
		canvas.rect(0,0,width,height);
		canvas.closePath();
		canvas.fill();

		canvas.fillStyle = '#FFF';
		canvas.beginPath();
		if(Player1.score == 7) {
			canvas.rect(0,0,width/2,height);
		} else {
			canvas.rect(width/2,0,width/2,height);
		}
		canvas.closePath();
		canvas.fill();
		canvas.fillStyle = "Red";
		canvas.font = "16pt Arial";
		canvas.fillText("GAME OVER", width/2+((Player1.score==7)?-250:10),height/2-50);
		canvas.fillText("THE WINNER IS: "+((Player1.score==7)?"Player1":"Player2"),width/2+((Player1.score==7)?-250:10),height/2-30);
	}, 100);
};

var GameLoop = function() {
	clear();
	checkCollision();

	Player1.draw();
	Player2.draw();
	MainBall.draw();

	if(state) {
		gLoop = setTimeout(GameLoop, 1000/50);
	}
};

GameLoop();