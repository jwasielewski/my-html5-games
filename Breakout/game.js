// game.js

var width = 1024;
var height = 600;

var gLoop;
var state = true;

var blip = document.getElementById('blip');

var c = document.getElementById('c');
var canvas = c.getContext('2d');

c.width = width;
c.height = height;

var clear = function() {
	canvas.fillStyle = '#000';
	canvas.beginPath();
	canvas.rect(0,0,width,height);
	canvas.closePath();
	canvas.fill();
};

var Player = function() {
	var self = this;

	self.pWidth = 100;
	self.pHeight = 5;
	self.X = 462;
	self.Y = 582;
	self.speed = 5;

	self.setPosition = function(x,y) {
		self.X = x;
		self.Y = y;
	};

	self.draw = function() {
		canvas.fillStyle = "#FFF";
		canvas.beginPath();
		canvas.rect(self.X,self.Y,self.pWidth,self.pHeight);
		canvas.closePath();
		canvas.fill();
	};

	self.move = function(x) {
		if(x + self.pWidth > width) {
			self.setPosition(width - self.pWidth, self.Y);
		} else {
			self.setPosition(x, self.Y);
		}
	};
};

var Ball = function(cX,cY) {
	var self = this;

	self.r = 4;
	self.coordX = cX;
	self.coordY = cY;
	self._cX = cX;
	self._cY = cY;
	self.speed = 15;
	self.vx = 0.3;
	self.vy = -0.3;

	self.draw = function() {
		canvas.fillStyle = '#FFF';
		canvas.beginPath();
		canvas.arc(self.coordX, self.coordY, self.r, 0, Math.PI*2, false);
		canvas.closePath();
		canvas.fill();
		self.update();
	};

	self.update = function() {
		self.coordX += self.vx * (1000/50);
		self.coordY += self.vy * (1000/50);
	};

	self.reset = function() {
		self.coordX = self._cX;
		self.coordY = self._cY;
		self.vy = -self.vy;
	};
};

var Platform = function(x,y,c,f) {
	var self = this;

	self.X = x;
	self.Y = y;
	self.pWidth = 100;
	self.pHeight = 25;
	self.isAlive = true;
	self.life = 3;
	self.frame = 0;
	self.maxframes = f;
	self.colors = c;

	self.draw = function() {
		canvas.fillStyle = self.colors[self.frame];
		canvas.beginPath();
		canvas.rect(self.X,self.Y,self.pWidth,self.pHeight);
		canvas.closePath();
		canvas.fill();
		canvas.strokeStyle = "#000";
		canvas.strokeRect(self.X,self.Y,self.pWidth,self.pHeight);
	};

	self.checkCollision = function(x,y) {
		if((self.X<=x+ball.r && x+ball.r<=self.X+self.pWidth)
			&& (self.Y<=y+ball.r && y+ball.r<=self.Y+self.pHeight)) {
			if(self.frame == f)	{ self.isAlive = false; pmgr.numberOfPlatforms--; }
			else self.frame++;
			if(self.Y <= y + ball.r || self.Y + self.pHeight >= y - ball.r) {
				// if(self.Y < ball.coordY) ball.coordY = self.Y + self.pHeight + ball.r;
				// else 
				ball.vy = -ball.vy; // górna i dolna
			} else if (self.X <= x + ball.r || self.X + self.pWidth >= x - ball.r) {
				ball.vx = -ball.vx;
			}
			blip.play();
			return true;
		}
	};
};

var PlatformManager = function(x,y) {
	var self = this;

	self.platforms = [];
	self.numberOfPlatforms = 0;
	self.sX = x;
	self.sY = y;

	self.draw = function() {
		self.platforms.forEach(function(e) {
			if(e.isAlive) e.draw();
		});
	};

	self.checkCollision = function(x,y) {
		self.platforms.forEach(function(e) {
			if(e.isAlive) if(e.checkCollision(x,y)) return;
		});
		if(self.numberOfPlatforms == 0) {
			clearTimeout(gLoop);
		}
	};

	self.generatePlatforms = function() {
		var c0 = ["#470074","#612587","#774498"];
		var c1 = ["#a7a904","#87881f"];
		var c2 = ["#197400"];
		for(var y = self.sY; y < self.sY+(4*25); y += 25) {
			for(var x = self.sX; x < self.sX+(10*100); x += 100) {
				self.platforms.push(new Platform(x,y,c0,2));
				self.numberOfPlatforms++;
			}
		}
		for(var y = self.sY+(4*25); y < self.sY+(8*25); y += 25) {
			for(var x = self.sX; x < self.sX+(10*100); x += 100) {
				self.platforms.push(new Platform(x,y,c1,1));
				self.numberOfPlatforms++;
			}
		}
		for(var y = self.sY+(8*25); y < self.sY+(12*25); y += 25) {
			for(var x = self.sX; x < self.sX+(10*100); x += 100) {
				self.platforms.push(new Platform(x,y,c2,0));
				self.numberOfPlatforms++;
			}
		}
	};
};

var checkCollision = function() {

	if((player.X<=ball.coordX+ball.r && ball.coordX+ball.r<=player.X+player.pWidth)
		&& (player.Y<=ball.coordY+ball.r && ball.coordY+ball.r<=player.Y+player.pHeight)) {
		ball.vy = -ball.vy;
	}

	// dolna i górna ściana
	if(ball.coordY + ball.r > height) {
		// ball.vy = -ball.vy;
		// ball.coordY = height - ball.r;
		ball.reset();
	} else if (ball.coordY - ball.r < 0) {
		ball.vy = -ball.vy;
		ball.coordY = ball.r;
	}
	//lewa i prawa ściana
	if(ball.coordX < 0) {
		ball.vx = -ball.vx;
		ball.coordX = ball.r;
	} else if(ball.coordX + ball.r > width) {
		ball.vx = -ball.vx;
		ball.coordX = width - ball.r;
	}
};

document.onmousemove = function(e) {
	if(0 <= e.pageX - c.offsetLeft && e.pageX - c.offsetLeft <= width) {
		player.move(e.pageX - c.offsetLeft);
	}
};

var player = new Player;
var ball = new Ball(462,510);
var pmgr = new PlatformManager(12,30);
pmgr.generatePlatforms();

var GameLoop = function() {
	clear();
	checkCollision();
	pmgr.checkCollision(ball.coordX, ball.coordY);
	player.draw();
	ball.draw();
	pmgr.draw();
	if(state) {
		gLoop = setTimeout(GameLoop, 1000/50);
	}
};

GameLoop();